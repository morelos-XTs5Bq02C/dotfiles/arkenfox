/* override recipe: enable session restore ***/
user_pref("browser.startup.page", 3); // 0102
  // user_pref("browser.privatebrowsing.autostart", false); // 0110 required if you had it set as true
  // user_pref("browser.sessionstore.privacy_level", 0); // 1003 optional to restore cookies/formdata
user_pref("privacy.clearOnShutdown.history", false); // 2811
  // user_pref("privacy.cpd.history", false); // 2820 optional to match when you use Ctrl-Shift-Del

// Disable Pocket
user_pref("extensions.pocket.enabled", false);

// Disabled WebRTC
user_pref("media.peerconnection.ice.default_address_only", true);
user_pref("media.peerconnection.enabled", false);

// Disable connection tests
user_pref("network.captive-portal-service.enabled", false);

// Disable telemetry
user_pref("toolkit.telemetry.enabled", false);

// Do Not Track
user_pref("privacy.donottrackheader.enabled", true);

// Encrypted Friend Hello
user_pref("network.dns.echconfig.enabled", true);
user_pref("network.dns.http3_echconfig.enabled", true);
user_pref("network.trr.mode", 3);

// Disable Safe Browsing
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);

// Disable WASM
user_pref("javascript.options.wasm", false);
user_pref("javascript.options.wasm_baselinejit", false);
user_pref("javascript.options.wasm_ionjit", false);

// Enable the Legacy Toolkit
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
